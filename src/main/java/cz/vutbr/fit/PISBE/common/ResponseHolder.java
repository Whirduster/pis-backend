package cz.vutbr.fit.PISBE.common;

import cz.vutbr.fit.PISBE.jsonObjects.EmptyJsonResponse;
import cz.vutbr.fit.PISBE.jsonObjects.ResponseBodyLoginJackson;
import cz.vutbr.fit.PISBE.jsonObjects.ResponseBodyMessageJackson;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class ResponseHolder<T> {
    private int returnCode;
    private T responseBody;

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public void setResponseBody(T responseBody) {
        this.responseBody = responseBody;
    }

    public T getResponseBody() {
        return responseBody;
    }

    public T setup(HttpServletResponse response) {
        response.setStatus(returnCode);
        return responseBody;
    }

    public static ResponseHolder returnEmptyBody(int code){
        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setResponseBody(new EmptyJsonResponse());
        responseHolder.setReturnCode(code);
        return responseHolder;
    }

    public static ResponseHolder returnEmptyArray(int code){
        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setReturnCode(code);
        ArrayList<Object> emptyArray = new ArrayList<>();
        responseHolder.setResponseBody(emptyArray);
        return responseHolder;
    }

    public static ResponseHolder returnOkAndMessage(String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }

    public static ResponseHolder returnBadRequestAndMessage(String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_BAD_REQUEST);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }

    public static ResponseHolder returnReturnForbiddenAndMessage(String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_FORBIDDEN);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }

    public static ResponseHolder returnReturnCodeAndMessage(int returnCode, String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(returnCode);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }

    public static ResponseHolder loginResponse(int returnCode, String token, List<String> roles, String message) {
        ResponseHolder<ResponseBodyLoginJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(returnCode);
        responseHolder.setResponseBody(new ResponseBodyLoginJackson(token, roles, message));
        return responseHolder;
    }
}
