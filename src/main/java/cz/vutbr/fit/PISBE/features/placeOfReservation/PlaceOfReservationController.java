package cz.vutbr.fit.PISBE.features.placeOfReservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
public class PlaceOfReservationController {

    @Autowired
    PlaceOfReservationServiceDao placeOfReservationServiceDao;

    @GetMapping(value = "schemes/tables")
    public Object getTablesSchemes(HttpServletResponse response) {
        return placeOfReservationServiceDao.getTablesSchemes("TABLE").setup(response);
    }

    @GetMapping(value = "schemes/lounges")
    public Object getLoungesSchemes(HttpServletResponse response) {
        return placeOfReservationServiceDao.getTablesSchemes("LOUNGE").setup(response);
    }

    @PostMapping(value = "reservations/logged/free-places")
    public Object getFreeTables(@RequestBody String json, HttpServletResponse response){
        return placeOfReservationServiceDao.getFreeTables(json).setup(response);
    }
}
