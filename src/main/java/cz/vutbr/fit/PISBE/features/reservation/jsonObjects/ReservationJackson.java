package cz.vutbr.fit.PISBE.features.reservation.jsonObjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.vutbr.fit.PISBE.features.user.jsonObjects.HostInfoJackson;

import java.util.Date;

public class ReservationJackson {
    private int reservationId;
    private String placeType;
    private int placeId;
    private int expectedTime;
    private Date date;
    private String reservationState;
    private HostInfoJackson hostInfo;

    public ReservationJackson() {
    }

    public ReservationJackson(String reservationState){
        this.reservationState = reservationState;
    }

    public ReservationJackson(int reservationId, String placeType, int placeId, int expectedTime, Date date, String reservationState, HostInfoJackson hostInfo) {
        this.reservationId = reservationId;
        this.placeType = placeType;
        this.placeId = placeId;
        this.expectedTime = expectedTime;
        this.date = date;
        this.reservationState = reservationState;
        this.hostInfo = hostInfo;
    }

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public String getPlaceType() {
        return placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public int getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(int expectedTime) {
        this.expectedTime = expectedTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReservationState() {
        return reservationState;
    }

    public void setReservationState(String reservationState) {
        this.reservationState = reservationState;
    }

    public HostInfoJackson getHostInfo() {
        return hostInfo;
    }

    public void setHostInfo(HostInfoJackson hostInfo) {
        this.hostInfo = hostInfo;
    }
}
