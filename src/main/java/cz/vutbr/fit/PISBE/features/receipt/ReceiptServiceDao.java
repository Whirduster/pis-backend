package cz.vutbr.fit.PISBE.features.receipt;

import cz.vutbr.fit.PISBE.common.ResponseHolder;

import java.text.ParseException;

public interface ReceiptServiceDao {

    ResponseHolder getBillingForm(int placeId);

    ResponseHolder makePayment(String requestBody, int placeId) throws ParseException;

}
