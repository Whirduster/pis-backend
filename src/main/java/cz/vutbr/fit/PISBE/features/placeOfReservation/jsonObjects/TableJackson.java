package cz.vutbr.fit.PISBE.features.placeOfReservation.jsonObjects;

public class TableJackson {
    private int placeId;
    private int capacity;

    public TableJackson() {
    }

    public TableJackson(int placeId, int capacity) {
        this.placeId = placeId;
        this.capacity = capacity;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
