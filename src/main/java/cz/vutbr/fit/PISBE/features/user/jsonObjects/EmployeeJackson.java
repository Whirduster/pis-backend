package cz.vutbr.fit.PISBE.features.user.jsonObjects;

import cz.vutbr.fit.PISBE.dto.RolesList;
import java.util.ArrayList;


public class EmployeeJackson{
    private EmployeeInfo employeeInfo;
    private ArrayList<RolesList> roles;

    public EmployeeJackson() {
    }

    public EmployeeJackson(EmployeeInfo employeeInfo, ArrayList<RolesList> roles) {
        this.employeeInfo = employeeInfo;
        this.roles = roles;
    }

    public EmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(EmployeeInfo employeeInfo) {
        this.employeeInfo = employeeInfo;
    }

    public ArrayList<RolesList> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<RolesList> roles) {
        this.roles = roles;
    }
}


