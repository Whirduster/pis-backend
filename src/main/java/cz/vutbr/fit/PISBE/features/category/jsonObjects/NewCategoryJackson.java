package cz.vutbr.fit.PISBE.features.category.jsonObjects;

public class NewCategoryJackson {
    String name;

    public NewCategoryJackson() {
    }

    public NewCategoryJackson(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
