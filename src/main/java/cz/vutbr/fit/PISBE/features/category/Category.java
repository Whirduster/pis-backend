package cz.vutbr.fit.PISBE.features.category;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Category")
public class Category {
    @Id
    @NotNull(message = "ID kategorie je povinné.")
    @SequenceGenerator(name = "CategoryIdGenerator", sequenceName = "CATEGORY_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CategoryIdGenerator")
    @Column(name = "CategoryID")
    private int categoryId;

    @NotNull
    @Column(name = "name", unique = true)
    @Size(max=50, message = "Název může mít maximálně 50 znaků.")
    private String name;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
