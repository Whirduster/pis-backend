package cz.vutbr.fit.PISBE.features.orderItem;

import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {
    @Query(value = "select * from ORDER_ITEM where PLACE_OF_RESERVATION_REFERENCE = ?1", nativeQuery = true)
    List<OrderItem> findAllByPlaceOfReservationReference(PlaceOfReservation placeOfReservation);

    OrderItem findByOrderItemId(int orderItemId);

    @Query(value = "select * from ORDER_ITEM where PLACE_OF_RESERVATION_REFERENCE = ?2 and MENU_ITEM_REFERENCE in (select MENU_ITEMID from MENU_ITEM where CATEGORY_REFERENCE = ?1)", nativeQuery = true)
    List<OrderItem> findAllByCategoryIdAndPlaceId(int categoryId, int placeId);

    @Query(value = "select * from ORDER_ITEM ORDER BY CREATED_AT", nativeQuery = true)
    List<OrderItem> findAllOrOrderByCreatedAt();
}
