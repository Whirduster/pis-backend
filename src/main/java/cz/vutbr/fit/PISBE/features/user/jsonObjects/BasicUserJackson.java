package cz.vutbr.fit.PISBE.features.user.jsonObjects;

public class BasicUserJackson {

    private String email;
    private String password;

    public BasicUserJackson() {
    }

    public BasicUserJackson(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
