package cz.vutbr.fit.PISBE.features.orderItem;

import cz.vutbr.fit.PISBE.features.menuItem.MenuItem;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservation;
import cz.vutbr.fit.PISBE.features.receipt.Receipt;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "OrderItem")
public class OrderItem {
    @Id
    @NotNull(message = "ID položky objednávky je povinné")
    @SequenceGenerator(name = "OrderIdGenerator", sequenceName = "ORDER_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =  "OrderIdGenerator")
    @Column(name = "orderItemID")
    private int orderItemId;

    @Column(name = "specialRequirement", nullable = true)
    @Size(max = 300, message = "Speciální požadavky mohou mít maximálně 300 znaků!")
    private String specialRequirement;

    @NotNull(message = "Stav objednávky nebyl zadán!")
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "createdAt")
    private Timestamp createdAt;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "placeOfReservationReference", referencedColumnName = "placeID")
    private PlaceOfReservation placeOfReservationReference;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "ReceiptReference", referencedColumnName = "ReceiptID")
    private Receipt receiptReference;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "menuItemReference", referencedColumnName = "menuItemID")
    private MenuItem menuItemReference;

    public OrderItem() {
    }

    public OrderItem(String specialRequirement, State state, Timestamp createdAt) {
        this.specialRequirement = specialRequirement;
        this.state = state;
        this.createdAt = createdAt;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getSpecialRequirement() {
        return specialRequirement;
    }

    public void setSpecialRequirement(String specialRequirement) {
        this.specialRequirement = specialRequirement;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public String getCreatedAtFormatted(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.createdAt);
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public PlaceOfReservation getPlaceOfReservationReference() {
        return placeOfReservationReference;
    }

    public void setPlaceOfReservationReference(PlaceOfReservation placeOfReservationReference) {
        this.placeOfReservationReference = placeOfReservationReference;
    }

    public MenuItem getMenuItemReference() {
        return menuItemReference;
    }

    public void setMenuItemReference(MenuItem menuItemReference) {
        this.menuItemReference = menuItemReference;
    }

    public Receipt getReceiptReference() {
        return receiptReference;
    }

    public void setReceiptReference(Receipt receiptReference) {
        this.receiptReference = receiptReference;
    }
}
