package cz.vutbr.fit.PISBE.features.receipt.jsonObjects;

import java.util.ArrayList;

public class PaymentJackson {
    private ArrayList<Integer> orderItemIds;

    public PaymentJackson() {
    }

    public PaymentJackson(ArrayList<Integer> orderItemIds) {
        this.orderItemIds = orderItemIds;
    }

    public ArrayList<Integer> getOrderItemIds() {
        return orderItemIds;
    }

    public void setOrderItemIds(ArrayList<Integer> orderItemIds) {
        this.orderItemIds = orderItemIds;
    }
}
