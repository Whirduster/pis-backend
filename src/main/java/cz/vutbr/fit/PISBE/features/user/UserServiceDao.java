package cz.vutbr.fit.PISBE.features.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.features.role.Role;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public interface UserServiceDao {

    void saveUserWitchEncryptedPassword(User user);

    ResponseHolder getAllEmployees();

    ResponseHolder createNewUserFromJson(String json);

    ResponseHolder loginUserFromJson(String json);

    ResponseHolder addNewEmployeeFromJson(String json);

    ResponseHolder deleteEmployee(String email);

    ResponseHolder editEmployee(String json, String email) throws JsonProcessingException;

    ResponseHolder editEmployeesRole(String json, String email) throws JsonProcessingException;

    ArrayList<String> getRolesAsString(Collection<Role> userRoles);

    boolean canAccess(String rolesWithAuthorization);

    Set<String> parseRolesFromStringList(String roles);

    ResponseHolder getUserInfo();

    ResponseHolder editUserAccount(String requestBody, String email);

}
