package cz.vutbr.fit.PISBE.features.receipt.jsonObjects;

import cz.vutbr.fit.PISBE.features.orderItem.State;

public class ReceiptJackson {
    private int orderItemId;
    private int menuItemId;
    private String name;
    private double price;
    private String specialRequirements;
    private String grammage;
    private String state;

    public ReceiptJackson() {
    }

    public ReceiptJackson(int orderItemId, int menuItemId, String name, double price, String specialRequirements, String grammage) {
        this.orderItemId = orderItemId;
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.specialRequirements = specialRequirements;
        this.grammage = grammage;
    }

    public ReceiptJackson(int orderItemId, int menuItemId, String name, double price, String specialRequirements, String grammage,
                          State state) {
        this.orderItemId = orderItemId;
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.specialRequirements = specialRequirements;
        this.grammage = grammage;
        this.state = state.name();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSpecialRequirements() {
        return specialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        this.specialRequirements = specialRequirements;
    }

    public String getGrammage() {
        return grammage;
    }

    public void setGrammage(String grammage) {
        this.grammage = grammage;
    }
}
