package cz.vutbr.fit.PISBE.features.placeOfReservation;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.JsonToObjectParser;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.configuration.Auth;
import cz.vutbr.fit.PISBE.configuration.Principal;
import cz.vutbr.fit.PISBE.features.placeOfReservation.jsonObjects.OccupancyOverviewJackson;
import cz.vutbr.fit.PISBE.features.placeOfReservation.jsonObjects.TableJackson;
import cz.vutbr.fit.PISBE.features.reservation.ReservationRepository;
import cz.vutbr.fit.PISBE.features.reservation.jsonObjects.ReservationInfoJackson;
import cz.vutbr.fit.PISBE.features.user.UserServiceDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PlaceOfReservationServiceDaoImpl implements PlaceOfReservationServiceDao {

    @Autowired
    UserServiceDaoImpl userServiceDao;

    @Autowired
    PlaceOfReservationRepository placeOfReservationRepository;

    @Autowired
    ReservationRepository reservationRepository;

    public static final long HOUR = 3600 * 1000;

    public ResponseHolder getTablesSchemes(String type) {
        if (!userServiceDao.canAccess("Waiter,Logged user")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<PlaceOfReservation> tables = placeOfReservationRepository.findAll();
        ArrayList<TableJackson> tablesJackson = new ArrayList<>();

        for (PlaceOfReservation table : tables) {
            if (type.equals(table.getType().name())) {
                TableJackson tableJackson = new TableJackson(table.getPlaceID(), table.getNumberOfSeats());
                tablesJackson.add(tableJackson);
            }
        }

        ResponseHolder<ArrayList<TableJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(tablesJackson);
        return responseHolder;
    }

    public List<List<Integer>> occupancyOverview(ReservationInfoJackson reservationInfoJackson) {
        String dateString = reservationInfoJackson.getDate() + " " + reservationInfoJackson.getTime() + ":00";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int expectedLength = 0;
        if (reservationInfoJackson.getExpectedLength() == 0) {
            long hours = date.getHours();
            if (hours > 16) {
                expectedLength = (int) (23 - hours);
            } else {
                expectedLength = 6;
            }
        } else {
            expectedLength = reservationInfoJackson.getExpectedLength();
        }

        Date endDate = new Date(date.getTime() + expectedLength * HOUR);
        long controlOpenHours = endDate.getHours();
        if ((controlOpenHours > 23) || (date.getDay() != endDate.getDay())) {
            return null;
        }

        List<Integer> freePlaces = reservationRepository.getAllFreePlacesByPlaceInTime(date, endDate);
        List<Integer> occupiedTables = reservationRepository.getAllOccupiedPlacesByPlaceInTime(date, endDate);

        List<List<Integer>> result = new ArrayList<List<Integer>>();
        result.add(freePlaces);
        result.add(occupiedTables);

        return result;
    }

    public ResponseHolder getFreeTables(String json) {
        ReservationInfoJackson reservationInfoJackson;
        try {
            reservationInfoJackson = JsonToObjectParser.parseJsonToObject(json, ReservationInfoJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }

        Principal me = Auth.me();
        String actualUserEmail = me.getEmail();

        List<Integer> freePlaces = null;
        List<Integer> occupiedTables = null;

        if (userServiceDao.canAccess("Waiter")) {
            List<List<Integer>> overview = occupancyOverview(reservationInfoJackson);
            if (overview == null) {
                return ResponseHolder.returnBadRequestAndMessage("Vaše rezervace přesahuje otevírací dobu. Otevírací doba je do 23:00. Prosím upravte dobu nebo čas rezervace.");
            }
            freePlaces = overview.get(0);
            occupiedTables = overview.get(1);

            ArrayList<Integer> selectPlaces = new ArrayList<>();
            ArrayList<PlaceOfReservation> suitablePlaces = new ArrayList<>();
            for (int placeId : freePlaces) {
                Optional<PlaceOfReservation> place = placeOfReservationRepository.findById(placeId);
                suitablePlaces.add(place.get());
            }

            for (PlaceOfReservation suitablePlace : suitablePlaces) {
                if (reservationInfoJackson.getHostsCount() <= suitablePlace.getNumberOfSeats()) {
                    selectPlaces.add(suitablePlace.getPlaceID());
                }
            }

            freePlaces = selectPlaces;

        } else if (userServiceDao.canAccess("Logged user")) {
            List<List<Integer>> overview = occupancyOverview(reservationInfoJackson);
            if (overview == null) {
                return ResponseHolder.returnBadRequestAndMessage("Vaše rezervace přesahuje otevírací dobu. Otevírací doba je do 23:00. Prosím upravte dobu nebo čas rezervace.");
            }
            freePlaces = overview.get(0);
            occupiedTables = overview.get(1);

            ArrayList<PlaceOfReservation> suitablePlaces = new ArrayList<>();
            for (int placeId : freePlaces) {
                Optional<PlaceOfReservation> place = placeOfReservationRepository.findById(placeId);
                suitablePlaces.add(place.get());
            }
            ArrayList<Integer> selectPlaces = new ArrayList<>();

            suitablePlaces.sort(Comparator.comparing(PlaceOfReservation::getNumberOfSeats));

            //nabídne všechny volné sedadla tak aby byla max 2 volná místa. Pokud takové místo neexistuje nabídne další možný počet sedadel a všechny stoly s tímto počtem sedadel.
            // pro nabidnuti salonku musi byt obsazenost alespon polovicni
            for (PlaceOfReservation suitablePlace : suitablePlaces) {
                if (reservationInfoJackson.getHostsCount() <= suitablePlace.getNumberOfSeats()) {
                    if ((reservationInfoJackson.getHostsCount() - suitablePlace.getNumberOfSeats() < -2)) {

                        if ((selectPlaces.size() == 0) || (placeOfReservationRepository.getOne(selectPlaces.get(selectPlaces.size() - 1)).getNumberOfSeats() == suitablePlace.getNumberOfSeats())) {
                            if (selectPlaces.size() == 0) {
                                selectPlaces.add(suitablePlace.getPlaceID());
                            } else {
                                PlaceOfReservation actualPlace = placeOfReservationRepository.getOne(selectPlaces.get(selectPlaces.size() - 1));
                                if ((actualPlace.getType() == Type.LOUNGE) && (reservationInfoJackson.getHostsCount() > actualPlace.getNumberOfSeats() / 2) || ((actualPlace.getType() == Type.TABLE)))
                                    selectPlaces.add(suitablePlace.getPlaceID());
                            }
                        } else {
                            break;
                        }
                    }
                    selectPlaces.add(suitablePlace.getPlaceID());
                }
            }
            freePlaces = selectPlaces;
        } else {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        OccupancyOverviewJackson responseJson = new OccupancyOverviewJackson(freePlaces, occupiedTables);

        ResponseHolder<OccupancyOverviewJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(responseJson);
        return responseHolder;
    }
}
