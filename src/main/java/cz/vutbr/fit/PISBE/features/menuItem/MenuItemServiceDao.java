package cz.vutbr.fit.PISBE.features.menuItem;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ResponseHolder;

public interface MenuItemServiceDao {

    ResponseHolder getAllMenuItems();

    ResponseHolder getAllCategoriesForMenuItemForm();

    ResponseHolder createNewMenuItem(String requestBody);

    ResponseHolder getMenuItem(int menuItemId);

    ResponseHolder editMenuItem(String requestBody, int menuItemId);

    ResponseHolder deleteMenuItem(int menuItemId);

    ResponseHolder editMenuPost(String requestBody);

    ResponseHolder getSalesOfMenuItems();

    ResponseHolder getSalesForPeriod(int year, int month);

    ResponseHolder getTimesOfSale();

    ResponseHolder getUtilization();

    ResponseHolder getLastTimeSold();
}
