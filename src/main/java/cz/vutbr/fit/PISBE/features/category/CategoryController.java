package cz.vutbr.fit.PISBE.features.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class CategoryController {
    @Autowired
    CategoryServiceDao categoryServiceDao;

    @GetMapping(value = "/menu")
    public Object getMenu(HttpServletResponse response){
        return categoryServiceDao.getMenu().setup(response);
    }

    @GetMapping(value = "/menu/edit")
    public Object getMenuDetails(HttpServletResponse response){
        return categoryServiceDao.getMenuDetails().setup(response);
    }

    @GetMapping(value = "/menu/categories")
    public Object getAllCategories(HttpServletResponse response){
        return categoryServiceDao.getAllCategories().setup(response);
    }

    @PostMapping(value = "/menu/categories")
    public Object createNewCategory(HttpServletResponse response, @RequestBody String requestBody){
        return categoryServiceDao.createNewCategory(requestBody).setup(response);
    }

    @DeleteMapping(value = "/menu/categories/{categoryName}")
    public Object deleteCategory(HttpServletResponse response, @PathVariable String categoryName){
        return categoryServiceDao.deleteCategory(categoryName).setup(response);
    }
}
