package cz.vutbr.fit.PISBE.features.placeOfReservation.jsonObjects;

import java.util.List;

public class OccupancyOverviewJackson {
    private List<Integer> freeTables;
    private List<Integer> occupiedTables;

    public OccupancyOverviewJackson() {
    }

    public OccupancyOverviewJackson(List<Integer> freeTables, List<Integer> occupiedTables) {
        this.freeTables = freeTables;
        this.occupiedTables = occupiedTables;
    }

    public List<Integer> getFreeTables() {
        return freeTables;
    }

    public void setFreeTables(List<Integer> freeTables) {
        this.freeTables = freeTables;
    }

    public List<Integer> getOccupiedTables() {
        return occupiedTables;
    }

    public void setOccupiedTables(List<Integer> occupiedTables) {
        this.occupiedTables = occupiedTables;
    }
}

