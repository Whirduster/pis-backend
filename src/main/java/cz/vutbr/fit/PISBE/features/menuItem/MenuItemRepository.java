package cz.vutbr.fit.PISBE.features.menuItem;

import cz.vutbr.fit.PISBE.features.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Integer> {
    MenuItem findByMenuItemId(int menuItemId);

    List<MenuItem> findAllByCategoryReference(Category categoryId);

    @Query(value = "select PRICE, PRODUCTION_PRICE, IN_MENU, DESCRIPTION, GRAMMAGE, NAME, MENU_ITEMID, count(MENU_ITEMID) from ORDER_ITEM join MENU_ITEM on ORDER_ITEM.MENU_ITEM_REFERENCE = MENU_ITEM.MENU_ITEMID where STATE='PAID' group by PRICE, PRODUCTION_PRICE, IN_MENU, DESCRIPTION, GRAMMAGE, NAME, MENU_ITEMID", nativeQuery = true)
    List<Object> getCountOfSoldMenuItems();

    @Query(value = "select CREATED_AT, PRICE, PRODUCTION_PRICE, IN_MENU, DESCRIPTION, GRAMMAGE, NAME, MENU_ITEMID from ORDER_ITEM join MENU_ITEM on ORDER_ITEM.MENU_ITEM_REFERENCE = MENU_ITEM.MENU_ITEMID where STATE='PAID' group by PRICE, PRODUCTION_PRICE, IN_MENU, DESCRIPTION, GRAMMAGE, NAME, MENU_ITEMID, CREATED_AT", nativeQuery = true)
    List<Object> getAllOrders();

    @Query(value = "select ORDER_ITEMID from ORDER_ITEM join MENU_ITEM on ORDER_ITEM.MENU_ITEM_REFERENCE = MENU_ITEM.MENU_ITEMID where STATE='PAID'", nativeQuery = true)
    List<Integer> getAllOrdersOnlyId();

    @Query(value = "select PRICE, PRODUCTION_PRICE, IN_MENU, DESCRIPTION, GRAMMAGE, NAME, MENU_ITEMID, CREATED_AT from ORDER_ITEM join MENU_ITEM on ORDER_ITEM.MENU_ITEM_REFERENCE = MENU_ITEMID", nativeQuery=true)
    List<Object> getTimesOfSale();
}
