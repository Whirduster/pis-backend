package cz.vutbr.fit.PISBE.features.placeOfReservation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PlaceOfReservation")
public class PlaceOfReservation {
    @Id
    @NotNull(message = "ID místa je povinné'")
    @SequenceGenerator(name = "PlaceIdGenerator", sequenceName = "PLACE_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =  "PlaceIdGenerator")
    @Column(name = "placeID")
    private int placeID;

    @NotNull(message = "Kapacita místa nebyla zadána!")
    @Column(name = "numberOfSeats")
    private int numberOfSeats;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private Type type;

    public PlaceOfReservation() {
    }

    public PlaceOfReservation(int numberOfSeats, Type type) {
        this.numberOfSeats = numberOfSeats;
        this.type = type;
    }

    public int getPlaceID() {
        return placeID;
    }

    public void setPlaceID(int placeID) {
        this.placeID = placeID;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}


