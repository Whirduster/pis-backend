package cz.vutbr.fit.PISBE.features.receipt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

@RestController
public class ReceiptController {

    @Autowired
    ReceiptServiceDao receiptServiceDao;

    @GetMapping(value = "places/{placeId:[\\d]+}/payment")
    public Object getBillingForm(HttpServletResponse response,  @PathVariable int placeId) {
        return receiptServiceDao.getBillingForm(placeId).setup(response);
    }

    @PostMapping(value = "places/{placeId:[\\d]+}/payment")
    public Object makePayment(HttpServletResponse response, @RequestBody String requestBody, @PathVariable int placeId) throws ParseException {
        return receiptServiceDao.makePayment(requestBody, placeId).setup(response);
    }


}
