package cz.vutbr.fit.PISBE.features.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    ArrayList<User> findAll();
    User findByEmail(String email);

    List<User> findAllByEmail(String email);
}
