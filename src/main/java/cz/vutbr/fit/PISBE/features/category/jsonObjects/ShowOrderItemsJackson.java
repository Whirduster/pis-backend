package cz.vutbr.fit.PISBE.features.category.jsonObjects;

import cz.vutbr.fit.PISBE.features.orderItem.jsonObjects.OrderItemJackson;

import java.util.ArrayList;

public class ShowOrderItemsJackson {
    private String categoryName;
    private ArrayList<OrderItemJackson> orderItems;

    public ShowOrderItemsJackson() {
    }

    public ShowOrderItemsJackson(String categoryName, ArrayList<OrderItemJackson> orderItems) {
        this.categoryName = categoryName;
        this.orderItems = orderItems;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<OrderItemJackson> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItemJackson> orderItems) {
        this.orderItems = orderItems;
    }
}
