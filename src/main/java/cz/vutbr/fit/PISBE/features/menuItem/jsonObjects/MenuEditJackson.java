package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

public class MenuEditJackson {
    private int menuItemId;

    public MenuEditJackson() {
    }

    public MenuEditJackson(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }
}
