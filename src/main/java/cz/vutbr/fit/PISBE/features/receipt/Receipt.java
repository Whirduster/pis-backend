package cz.vutbr.fit.PISBE.features.receipt;

import cz.vutbr.fit.PISBE.features.menuItem.MenuItem;
import cz.vutbr.fit.PISBE.features.orderItem.OrderItem;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "Receipt")
public class Receipt {
    @Id
    @NotNull(message = "ID účtenky je povinné")
    @SequenceGenerator(name = "ReceiptIdGenerator", sequenceName = "RECEIPT_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =  "ReceiptIdGenerator")
    @Column(name = "receiptID")
    private int receiptId;

    @NotNull(message = "Čas vytvoření je povinný")
    @Column(name = "createdAt")
    private Date dateOfPosting;

    public Receipt() {
    }

    public Receipt(Date dateOfPosting) {
        this.dateOfPosting = dateOfPosting;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int idReceipt) {
        this.receiptId = idReceipt;
    }

    public Date getDateOfPosting() {
        return dateOfPosting;
    }

    public void setDateOfPosting(Date dateOfPosting) {
        this.dateOfPosting = dateOfPosting;
    }

}
