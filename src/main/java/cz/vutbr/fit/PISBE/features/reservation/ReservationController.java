package cz.vutbr.fit.PISBE.features.reservation;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class ReservationController {

    @Autowired
    ReservationServiceDao reservationServiceDao;

    @GetMapping(value = "/reservations")
    public Object getAllReservations(HttpServletResponse response) {
        return reservationServiceDao.getAllReservations().setup(response);
    }

    @PostMapping(value = "/reservations/new/unlogged")
    public Object addNewReservation(@RequestBody String json, HttpServletResponse response) {
        return reservationServiceDao.addNewReservation(json).setup(response);
    }

    @PostMapping(value = "/reservations/new/scheme")
    public Object addNewReservationForLoggedUserAndWaiter(@RequestBody String json, HttpServletResponse response){
        return reservationServiceDao.addNewReservationForLoggedUserAndWaiter(json).setup(response);
    }

    @PutMapping(value = "/reservations/{reservationId:[\\d]+}")
    public Object editReservation(HttpServletResponse response, @PathVariable int reservationId, @RequestBody String requestBody){
        return reservationServiceDao.editReservation(reservationId, requestBody).setup(response);
    }

    @PostMapping(value = "/reservations/code")
    public Object cancelReservationByCode(HttpServletResponse response, @RequestBody String requestBody) throws JsonProcessingException {
        return reservationServiceDao.cancelReservationByCode(requestBody).setup(response);
    }
}
