package cz.vutbr.fit.PISBE.features.orderItem.jsonObjects;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class OrderItemJackson {
    private int orderItemId;
    private int menuItemId;
    private String name;
    private double price;
    private String specialRequirements;
    private String state;
    private String grammage;

    public OrderItemJackson() {
    }

    public OrderItemJackson(int orderItemId){
        this.orderItemId = orderItemId;
    }

    public OrderItemJackson(int orderItemId, int menuItemId, String name, double price, String specialRequirements, String state) {
        this.orderItemId = orderItemId;
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.specialRequirements = specialRequirements;
        this.state = state;
    }

    public OrderItemJackson(int orderItemId, int menuItemId, String name, double price, String specialRequirements,
                            String state, String grammage) {
        this.orderItemId = orderItemId;
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.specialRequirements = specialRequirements;
        this.state = state;
        this.grammage = grammage;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSpecialRequirements() {
        return specialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        this.specialRequirements = specialRequirements;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGrammage() {
        return grammage;
    }

    public void setGrammage(String grammage) {
        this.grammage = grammage;
    }
}
