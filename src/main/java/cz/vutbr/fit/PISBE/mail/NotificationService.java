package cz.vutbr.fit.PISBE.mail;

import cz.vutbr.fit.PISBE.features.reservation.Reservation;
import cz.vutbr.fit.PISBE.features.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Service
public class NotificationService {

    private JavaMailSender javaMailSender;

    @Autowired
    public NotificationService(JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
    }

    public void sendNotification(User user) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(user.getEmail());
        mail.setFrom("pisrestaurantvutfit@gmail.com");
        mail.setSubject("Vytvoření účtu zaměstnance");
        mail.setText("Vaše dočasné heslo pro přhlášení do systému je: " + user.getPassword() + "\n\n Prosím, změňte toto heslo při prvním přihlášení. \n\n Administrátorký tým pisRestaurant");
        javaMailSender.send(mail);
    }

    public void sendNotificationForReservation(String code, String email, Reservation reservation) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(email);
        mail.setFrom("pisrestaurantvutfit@gmail.com");
        mail.setSubject("Vytvoření rezervace pisRestaurant");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        mail.setText("Vaše rezervace v restauraci pisRestaurant byla vytvořena.\n\n Datum vaší rezervace:  " + dateFormat.format(reservation.getReservationDate()) + ".\n\n Váš kod pro zrušení rezervace je: " + code + "\n\n Administrátorký tým pisRestaurant");

        javaMailSender.send(mail);
    }
}
