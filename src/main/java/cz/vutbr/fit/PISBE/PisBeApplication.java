package cz.vutbr.fit.PISBE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PisBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PisBeApplication.class, args);
	}

}
