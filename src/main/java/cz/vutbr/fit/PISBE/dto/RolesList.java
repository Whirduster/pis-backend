package cz.vutbr.fit.PISBE.dto;

public class RolesList {
    private String name;
    private Boolean hasRole;

    public RolesList() {
    }

    public RolesList(String name, Boolean hasRole) {
        this.name = name;
        this.hasRole = hasRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasRole() {
        return hasRole;
    }

    public void setHasRole(Boolean hasRole) {
        this.hasRole = hasRole;
    }
}
